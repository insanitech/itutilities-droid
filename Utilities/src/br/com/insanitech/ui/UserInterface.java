package br.com.insanitech.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

public class UserInterface {
    public static <T> Dialog createAlert(Activity activity, T title, T message, T buttonTitle, final Runnable listener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        if (title.getClass().equals(Integer.class)) {
            dialog.setTitle((Integer) title);
        } else {
            dialog.setTitle((String) title);
        }
        if (message.getClass().equals(Integer.class)) {
            dialog.setMessage((Integer) message);
        } else {
            dialog.setMessage((String) message);
        }
        if (buttonTitle.getClass().equals(Integer.class)) {
            return dialog.setPositiveButton((Integer) buttonTitle, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (listener != null) {
                        listener.run();
                    }
                }
            }).create();
        }
        return dialog.setPositiveButton((String) buttonTitle, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (listener != null) {
                    listener.run();
                }
            }
        }).create();
    }

    @SuppressWarnings("unchecked")
    public static <T extends View> T findViewById(Activity ctx, int id) {
        return (T) ctx.findViewById(id);
    }

    @SuppressWarnings("unchecked")
    public static <T extends View> T findViewById(View view, int id) {
        return (T) view.findViewById(id);
    }

    public static float toComplexUnit(float value, DisplayMetrics metrics) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, metrics);
    }
}
