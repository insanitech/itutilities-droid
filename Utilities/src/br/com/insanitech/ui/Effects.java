package br.com.insanitech.ui;

import android.view.MotionEvent;
import android.view.View;

/**
 * Created by anderson on 19/10/15.
 */
public class Effects {
    public static void addAlphaEffect(View view) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.setAlpha(0.5f);
                        break;

                    case MotionEvent.ACTION_UP:
                        v.setAlpha(1.0f);
                        break;
                }

                return false;
            }
        });
    }
}
