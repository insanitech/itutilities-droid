package br.com.insanitech.ui.smartlayout.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import br.com.insanitech.ui.smartlayout.LayoutDefinitions;
import br.com.insanitech.ui.smartlayout.SmartLayout;

/**
 * Created by anderson on 15/07/16.
 */
public class LinearLayout extends android.widget.LinearLayout {
    private LayoutDefinitions definitions;

    public LinearLayout(Context context) {
        super(context);
    }

    public LinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        definitions = SmartLayout.setupAttributes(this, attrs);
    }

    public LinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        definitions = SmartLayout.setupAttributes(this, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        definitions = SmartLayout.setupAttributes(this, attrs);
    }

    private boolean converted = false;

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (!converted) {
            converted = true;
            SmartLayout.applyDefinitions(this, definitions);
        }
    }
}
