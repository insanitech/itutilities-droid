package br.com.insanitech.ui.smartlayout.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import br.com.insanitech.ui.smartlayout.LayoutDefinitions;
import br.com.insanitech.ui.smartlayout.SmartLayout;

/**
 * Created by anderson on 15/07/16.
 */
public class EditText extends android.widget.EditText {
    private LayoutDefinitions definitions;

    public EditText(Context context) {
        super(context);
    }

    public EditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        definitions = SmartLayout.setupAttributes(this, attrs);
    }

    public EditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        definitions = SmartLayout.setupAttributes(this, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        definitions = SmartLayout.setupAttributes(this, attrs);
    }

    private boolean converted = false;

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (!converted) {
            converted = true;
            SmartLayout.applyDefinitions(this, definitions);
            SmartLayout.applyTextSizeDefinitions(this, definitions);
        }
    }
}
