package br.com.insanitech.ui.smartlayout;

public class LayoutDefinitions {
    public static final int FLAG_WIDTH = 1;
    public static final int FLAG_HEIGHT = 2;
    public static final int FLAG_MARGIN = 4;
    public static final int FLAG_MARGIN_TOP = 8;
    public static final int FLAG_MARGIN_BOTTOM = 16;
    public static final int FLAG_MARGIN_LEFT = 32;
    public static final int FLAG_MARGIN_RIGHT = 64;
    public static final int FLAG_PADDING = 128;
    public static final int FLAG_PADDING_TOP = 256;
    public static final int FLAG_PADDING_BOTTOM = 512;
    public static final int FLAG_PADDING_LEFT = 1024;
    public static final int FLAG_PADDING_RIGHT = 2048;
    public static final int FLAG_TEXT_SIZE = 4096;
    protected boolean overrideWidth;
    protected boolean overrideHeight;
    protected boolean overrideMargin;
    protected boolean overrideMarginTop;
    protected boolean overrideMarginBottom;
    protected boolean overrideMarginLeft;
    protected boolean overrideMarginRight;
    protected boolean overridePadding;
    protected boolean overridePaddingTop;
    protected boolean overridePaddingBottom;
    protected boolean overridePaddingLeft;
    protected boolean overridePaddingRight;
    protected boolean overrideTextSize;

    protected LayoutDefinitions() {
    }

    public boolean isOverrideWidth() {
        return this.overrideWidth;
    }

    public boolean isOverrideHeight() {
        return this.overrideHeight;
    }

    public boolean isOverrideMargin() {
        return this.overrideMargin;
    }

    public boolean isOverrideMarginTop() {
        return this.overrideMarginTop;
    }

    public boolean isOverrideMarginBottom() {
        return this.overrideMarginBottom;
    }

    public boolean isOverrideMarginLeft() {
        return this.overrideMarginLeft;
    }

    public boolean isOverrideMarginRight() {
        return this.overrideMarginRight;
    }

    public boolean isOverridePadding() {
        return this.overridePadding;
    }

    public boolean isOverridePaddingTop() {
        return this.overridePaddingTop;
    }

    public boolean isOverridePaddingBottom() {
        return this.overridePaddingBottom;
    }

    public boolean isOverridePaddingLeft() {
        return this.overridePaddingLeft;
    }

    public boolean isOverridePaddingRight() {
        return this.overridePaddingRight;
    }

    public boolean isOverrideTextSize() {
        return this.overrideTextSize;
    }
}

