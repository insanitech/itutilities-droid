package br.com.insanitech.ui.smartlayout;

import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.insanitech.itsecurity.R;

import static br.com.insanitech.ui.smartlayout.LayoutDefinitions.*;

public class SmartLayout {
    private static float BASE_WIDTH = 480.0F;
    private static float BASE_HEIGHT = 800.0F;

    public SmartLayout() {
    }

    public static void setupSmartLayout(float width, float height) {
        BASE_WIDTH = width;
        BASE_HEIGHT = height;
    }

    public static float convertSizeValue(DisplayMetrics display, float value) {
        double baseHip = Math.sqrt(Math.pow((double)BASE_WIDTH, 2.0D) + Math.pow((double)BASE_HEIGHT, 2.0D));
        double currentHip = Math.sqrt(Math.pow((double)display.widthPixels, 2.0D) + Math.pow((double)display.heightPixels, 2.0D));
        float percentBase = (float)((double)value / baseHip);
        return (float)(currentHip * (double)percentBase);
    }

    public static LayoutDefinitions setupAttributes(View view, AttributeSet attrs) {
        if(!view.isInEditMode() && attrs != null) {
            TypedArray array = view.getContext().obtainStyledAttributes(attrs, R.styleable.OverrideAttributesAtRuntime);

            for(int i = 0; i < array.getIndexCount(); ++i) {
                if(array.getIndex(i) == R.styleable.OverrideAttributesAtRuntime_override) {
                    int flagValue = array.getInt(R.styleable.OverrideAttributesAtRuntime_override, 0);
                    LayoutDefinitions def = new LayoutDefinitions();
                    def.overrideWidth = (flagValue & FLAG_WIDTH) == FLAG_WIDTH;
                    def.overrideHeight = (flagValue & FLAG_HEIGHT) == FLAG_HEIGHT;
                    def.overrideMargin = (flagValue & FLAG_MARGIN) == FLAG_MARGIN;
                    def.overrideMarginTop = (flagValue & FLAG_MARGIN_TOP) == FLAG_MARGIN_TOP;
                    def.overrideMarginBottom = (flagValue & FLAG_MARGIN_BOTTOM) == FLAG_MARGIN_BOTTOM;
                    def.overrideMarginLeft = (flagValue & FLAG_MARGIN_LEFT) == FLAG_MARGIN_LEFT;
                    def.overrideMarginRight = (flagValue & FLAG_MARGIN_RIGHT) == FLAG_MARGIN_RIGHT;
                    def.overridePadding = (flagValue & FLAG_PADDING) == FLAG_PADDING;
                    def.overridePaddingTop = (flagValue & FLAG_PADDING_TOP) == FLAG_PADDING_TOP;
                    def.overridePaddingBottom = (flagValue & FLAG_PADDING_BOTTOM) == FLAG_PADDING_BOTTOM;
                    def.overridePaddingLeft = (flagValue & FLAG_PADDING_LEFT) == FLAG_PADDING_LEFT;
                    def.overridePaddingRight = (flagValue & FLAG_PADDING_RIGHT) == FLAG_PADDING_RIGHT;
                    def.overrideTextSize = (flagValue & FLAG_TEXT_SIZE) == FLAG_TEXT_SIZE;
                    return def;
                }
            }

            array.recycle();
        }

        return null;
    }

    public static void applyDefinitions(View view, LayoutDefinitions definitions) {
        if(definitions != null) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            DisplayMetrics dm = view.getResources().getDisplayMetrics();
            if(params != null) {
                if(definitions.isOverrideWidth()) {
                    params.width = (int)convertSizeValue(dm, (float)params.width);
                }

                if(definitions.isOverrideHeight()) {
                    params.height = (int)convertSizeValue(dm, (float)params.height);
                }

                if(params instanceof ViewGroup.MarginLayoutParams) {
                    ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams)params;
                    int topMargin = marginParams.topMargin;
                    int bottomMargin = marginParams.bottomMargin;
                    int leftMargin = marginParams.leftMargin;
                    int rightMargin = marginParams.rightMargin;
                    if(definitions.isOverrideMargin()) {
                        topMargin = (int)convertSizeValue(dm, (float)topMargin);
                        bottomMargin = (int)convertSizeValue(dm, (float)bottomMargin);
                        leftMargin = (int)convertSizeValue(dm, (float)leftMargin);
                        rightMargin = (int)convertSizeValue(dm, (float)rightMargin);
                    } else {
                        if(definitions.isOverrideMarginTop()) {
                            topMargin = (int)convertSizeValue(dm, (float)topMargin);
                        }

                        if(definitions.isOverrideMarginBottom()) {
                            bottomMargin = (int)convertSizeValue(dm, (float)bottomMargin);
                        }

                        if(definitions.isOverrideMarginLeft()) {
                            leftMargin = (int)convertSizeValue(dm, (float)leftMargin);
                        }

                        if(definitions.isOverrideMarginRight()) {
                            rightMargin = (int)convertSizeValue(dm, (float)rightMargin);
                        }
                    }

                    marginParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
                    view.setLayoutParams(params);
                }
            }

            int paddingTop = view.getPaddingTop();
            int paddingBottom = view.getPaddingBottom();
            int paddingLeft = view.getPaddingLeft();
            int paddingRight = view.getPaddingRight();
            if(definitions.isOverridePadding()) {
                paddingTop = (int)convertSizeValue(dm, (float)paddingTop);
                paddingBottom = (int)convertSizeValue(dm, (float)paddingBottom);
                paddingLeft = (int)convertSizeValue(dm, (float)paddingLeft);
                paddingRight = (int)convertSizeValue(dm, (float)paddingRight);
            } else {
                if(definitions.isOverridePaddingTop()) {
                    paddingTop = (int)convertSizeValue(dm, (float)paddingTop);
                }

                if(definitions.isOverridePaddingBottom()) {
                    paddingBottom = (int)convertSizeValue(dm, (float)paddingBottom);
                }

                if(definitions.isOverridePaddingLeft()) {
                    paddingLeft = (int)convertSizeValue(dm, (float)paddingLeft);
                }

                if(definitions.isOverridePaddingRight()) {
                    paddingRight = (int)convertSizeValue(dm, (float)paddingRight);
                }
            }

            view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
            view.requestLayout();
        }
    }

    public static void applyTextSizeDefinitions(TextView view, LayoutDefinitions definitions) {
        if(definitions != null && definitions.isOverrideTextSize()) {
            float textSize = view.getTextSize();
            float newTextSize = convertSizeValue(view.getResources().getDisplayMetrics(), textSize);
            view.setTextSize(0, newTextSize);
        }
    }
}

