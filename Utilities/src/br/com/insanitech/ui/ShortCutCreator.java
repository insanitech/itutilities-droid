package br.com.insanitech.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.preference.PreferenceManager;

public class ShortCutCreator {
	private static final String key = "shortcut_created";

	public static void createShortCut(Context ctx, Class<? extends Activity> startActivity, int icon_drawable, int shortcutName) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx.getApplicationContext());
		if (!prefs.getBoolean(key, false)) {
			Intent shortcutintent = new Intent(
				"com.android.launcher.action.INSTALL_SHORTCUT");
			shortcutintent.putExtra("duplicate", false);
			shortcutintent.putExtra(Intent.EXTRA_SHORTCUT_NAME, shortcutName);
			Parcelable icon = Intent.ShortcutIconResource.fromContext(ctx,
				icon_drawable);
			shortcutintent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);
			shortcutintent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, new Intent(
				ctx, startActivity));
			ctx.sendBroadcast(shortcutintent);

			prefs.edit().putBoolean(key, true).apply();
		}
	}

}
