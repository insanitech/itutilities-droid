package br.com.insanitech.info;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by anderson on 22/10/15.
 */
public class ApplicationInfos {
    public static String getApplicationVersionName(Context ctx) {
        try {
            PackageInfo pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();
        }
        return "0";
    }

    public static int getApplicationVersionCode(Context ctx) {
        try {
            PackageInfo pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            return pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public static String getApplicationName(Context ctx) {
        return ctx.getApplicationInfo().loadLabel(ctx.getPackageManager()).toString();
    }
}
