package br.com.insanitech.list;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;

public class KeyValueList extends ArrayList<KeyValuePair> {
    private static final long serialVersionUID = -3817654262938877634L;

    public KeyValueList() {

    }

    public KeyValueList(int capacity) {
        super(capacity);
    }

    public KeyValueList(Collection<KeyValuePair> collection) {
        super(collection);
    }

    public boolean add(String name, int value) {
        return super.add(new KeyValuePair(name, value));
    }

    public boolean add(String name, long value) {
        return super.add(new KeyValuePair(name, value));
    }

    public boolean add(String name, float value) {
        return super.add(new KeyValuePair(name, value));
    }

    public boolean add(String name, double value) {
        return super.add(new KeyValuePair(name, value));
    }

    public boolean add(String name, boolean value) {
        return super.add(new KeyValuePair(name, value));
    }

    public boolean add(String name, String value) {
        return super.add(new KeyValuePair(name, value));
    }

    @Override
    public String toString() {
        JSONArray array = new JSONArray();
        for (int i = 0; i < size(); i++) {
            array.put(get(i).toJson());
        }
        return array.toString();
    }

    public String toJsonString() throws JSONException {
        JSONObject object = new JSONObject();
        for (int i = 0; i < size(); i++) {
            object.put(get(i).getName(), get(i).getValue());
        }
        return object.toString();
    }

    public String toUrlEncodedString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            KeyValuePair pair = get(i);
            try {
                builder.append(URLEncoder.encode(pair.getName(), "UTF-8"));
                builder.append("=");
                builder.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }

            if (i < size() - 1) {
                builder.append("&");
            }
        }
        return builder.toString();
    }
}
