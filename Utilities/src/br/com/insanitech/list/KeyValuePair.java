package br.com.insanitech.list;

import org.json.JSONException;
import org.json.JSONObject;

public class KeyValuePair {
	private String name = null;
	private String value = null;

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public KeyValuePair(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public KeyValuePair(String name, int value) {
		this.name = name;
		this.value = String.valueOf(value);
	}

	public KeyValuePair(String name, long value) {
		this.name = name;
		this.value = String.valueOf(value);
	}

	public KeyValuePair(String name, float value) {
		this.name = name;
		this.value = String.valueOf(value);
	}

	public KeyValuePair(String name, double value) {
		this.name = name;
		this.value = String.valueOf(value);
	}

	public KeyValuePair(String name, Object value) {
		this.name = name;
		this.value = String.valueOf(value);
	}

	public KeyValuePair(String name, boolean value) {
		this.name = name;
		this.value = value ? "1" : "0";
	}

	public JSONObject toJson() {
		try {
			return new JSONObject().put(name, value);
		} catch (JSONException ex) {
			ex.printStackTrace();
		}
		return new JSONObject();
	}
}
