package br.com.insanitech.update;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;

import org.jsoup.Jsoup;

import java.io.IOException;

import br.com.insanitech.info.ApplicationInfos;
import br.com.insanitech.itsecurity.R;

/**
 * Created by anderson on 20/10/15.
 */
public class AppUpdateChecker {
    public class Version {
        private Activity context;
        private String newVersion;
        private String uriString;
    }

    public interface AppUpdateCheckerListener {
        void onUpdateResponse(Version version);
    }

    public static void checkForUpdates(final Activity context, final AppUpdateCheckerListener callback) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final String playStoreUriString = "https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en";
                    final String newVersion = Jsoup.connect(playStoreUriString)
                            .timeout(10000)
                            .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                            .referrer("http://www.google.com").get()
                            .select("div[itemprop=softwareVersion]").first()
                            .ownText();

                    if (isNewVersion(newVersion, ApplicationInfos.getApplicationVersionName(context))) {
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Version version = new AppUpdateChecker().new Version();
                                version.context = context;
                                version.newVersion = newVersion;
                                version.uriString = playStoreUriString;
                                if (callback != null) {
                                    callback.onUpdateResponse(version);
                                }
                            }
                        });
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public static boolean isNewVersion(String newVersion, String currentVersion) {
        String[] newVersionComponents = newVersion.split("[.]");
        String[] currentVersionCompoents = currentVersion.split("[.]");

        boolean isNewVersion = false;
        int position = 0;
        for (; position < newVersionComponents.length && position < currentVersionCompoents.length; position++) {
            isNewVersion = Integer.valueOf(newVersionComponents[position]) > Integer.valueOf(currentVersionCompoents[position]);
            if (isNewVersion) {
                break;
            }
        }

        if (!isNewVersion) {
            if (newVersionComponents.length > currentVersionCompoents.length) {
                isNewVersion = (position > 0 && Integer.valueOf(newVersionComponents[position - 1]).equals(Integer.valueOf(currentVersionCompoents[position - 1]))) &&
                        Integer.valueOf(newVersionComponents[newVersionComponents.length - 1]) > 0;
            }
        }

        return isNewVersion;
    }

    public static void showUpdateAlert(final Version version) {
        String appName = ApplicationInfos.getApplicationName(version.context);
        AlertDialog.Builder builder = new AlertDialog.Builder(version.context);
        builder.setTitle(R.string.util_update_title);
        builder.setMessage(String.format(version.context.getString(R.string.util_update_message_format), appName, version.newVersion));
        builder.setPositiveButton(R.string.util_update_now, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                version.context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(version.uriString)));
            }
        });
        builder.setNegativeButton(R.string.util_update_later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }
}
