package br.com.insanitech.io;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

import br.com.insanitech.list.KeyValueList;
import br.com.insanitech.list.KeyValuePair;

/**
 * Created by anderson on 17/11/15.
 */
public abstract class Request {
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_AUTHORIZATION = "Authorization";

    public static final String CONTENT_TYPE_TEXT_HTML = "text/html";
    public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
    public static final String CONTENT_TYPE_APPLICATION_URL_ENCODED = "application/x-www-form-urlencoded";

    public static final int STATUS_OK = 200;
    public static final int STATUS_MALFORMED_URL = -1;
    public static final int STATUS_CONNECTION_ERROR = 0;

    private KeyValueList headers = null;
    protected boolean isLoading = false;

    protected void addRequestHeaders(URLConnection connection) {
        if (headers != null) {
            for (KeyValuePair pair : headers) {
                connection.setRequestProperty(pair.getName(), pair.getValue());
            }

            Crashlytics.log(Log.DEBUG, getClass().getName(), "HTTP Headers: " + headers.toString());
        }
    }

    protected void readFromStreamAndDispatch(InputStream inputStream, int responseCode) throws IOException {
        BufferedInputStream stream = new BufferedInputStream(inputStream);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int read;
        do {
            read = stream.read(buffer, 0, 1024);
            if (read > 0) {
                output.write(buffer, 0, read);
            }
        } while (read != -1);

        stream.close();
        dispatchSuccess(output.toByteArray(), responseCode);
        output.close();
    }

    protected abstract void dispatchSuccess(byte[] data, int code);

    protected abstract void dispatchError(int code);

    public void setHeaderList(KeyValueList headers) {
        this.headers = headers;
    }

    public boolean isLoading() {
        return isLoading;
    }
}
