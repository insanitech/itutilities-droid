package br.com.insanitech.io;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class BasicJSONObject {
	protected JSONObject intern;

	public BasicJSONObject() {
		intern = new JSONObject();
	}

	public BasicJSONObject(JSONObject info) {
		intern = info;
	}

	public BasicJSONObject(BasicJSONObject other) {
		intern = other.intern;
	}

	protected <T extends Object> void jsonSetValue(String key, T value) {
		setValue(intern, key, value);
	}

	protected <T extends Object> void setValue(JSONObject in, String key, T value) {
		try {
			in.put(key, value);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	protected <T extends Object> T jsonGetValue(String key) {
		return getValue(intern, key);
	}
	
	@SuppressWarnings("unchecked")
	protected <T extends Object> T getValue(JSONObject in, String key) {
		try {
			return (T) in.get(key);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return null;
	}

	public JSONObject getJson() {
		return intern;
	}

	public void setJson(JSONObject object) {
		intern = object;
	}
}
