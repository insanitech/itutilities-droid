package br.com.insanitech.io;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by anderson on 10/11/15.
 */
public class Retainer extends Request {
    public interface RetainerListener {
        void onRetainResponse(Retainer retainer, int code, byte[] response);

        void onRetainFailed(Retainer retainer, int code);
    }

    public enum RetainerMethod {
        POST,
        PUT
    }

    private RetainerListener listener;
    private Thread requestThread;
    private RetainerMethod method = RetainerMethod.POST;

    private void retainWorker(final String url, final String content) {
        isLoading = true;
        requestThread = new Thread(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection conn;
                try {
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setConnectTimeout(10000);
                    conn.setReadTimeout(10000);
                } catch (MalformedURLException ex) {
                    ex.printStackTrace();
                    dispatchError(STATUS_MALFORMED_URL);
                    return;
                } catch (IOException ex) {
                    ex.printStackTrace();
                    dispatchError(STATUS_CONNECTION_ERROR);
                    return;
                }

                addRequestHeaders(conn);

                try {
                    if (method == RetainerMethod.POST) {
                        conn.setRequestMethod("POST");
                    } else if (method == RetainerMethod.PUT) {
                        conn.setRequestMethod("PUT");
                    }
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.connect();

                    BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new BufferedOutputStream(conn.getOutputStream())));
                    out.write(content);
                    out.flush();

                    readFromStreamAndDispatch(conn.getInputStream(), conn.getResponseCode());
                } catch (IOException ex) {
                    ex.printStackTrace();

                    try {
                        readFromStreamAndDispatch(conn.getErrorStream(), conn.getResponseCode());
                    } catch (IOException exc) {
                        exc.printStackTrace();
                        dispatchError(STATUS_CONNECTION_ERROR);
                    }
                } finally {
                    conn.disconnect();
                }
                requestThread = null;
            }
        });
        requestThread.start();
    }

    private void retainerWorkerFile(final String url, final String fieldName, final String fileName, final InputStream fileData) {
        isLoading = true;
        requestThread = new Thread(new Runnable() {
            @Override
            public void run() {
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "lakjnanKJA13KDGAEJGOE15IEOIG531NFA1Jdfj3213alfsj";

                HttpURLConnection conn;
                try {
                    conn = (HttpURLConnection) new URL(url).openConnection();
                } catch (MalformedURLException ex) {
                    ex.printStackTrace();
                    dispatchError(STATUS_MALFORMED_URL);
                    return;
                } catch (IOException ex) {
                    ex.printStackTrace();
                    dispatchError(STATUS_CONNECTION_ERROR);
                    return;
                }

                conn.setConnectTimeout(2 * 60 * 1000); // two minutes

                addRequestHeaders(conn);
                conn.addRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

                try {
                    if (method == RetainerMethod.POST) {
                        conn.setRequestMethod("POST");
                    } else if (method == RetainerMethod.PUT) {
                        conn.setRequestMethod("PUT");
                    }

                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    DataOutputStream out = new DataOutputStream(conn.getOutputStream());
                    out.writeBytes(twoHyphens + boundary + lineEnd);
                    out.writeBytes("Content-Disposition: form-data;name=\"" + fieldName + "\";filename=\"" + fileName + "\"");
                    out.writeBytes(lineEnd);
                    out.writeBytes("Content-Type: " + URLConnection.guessContentTypeFromName(fileName));
                    out.writeBytes(lineEnd);
                    out.writeBytes("Content-Transfer-Encoding: binary");
                    out.writeBytes(lineEnd);
                    out.writeBytes(lineEnd);
                    out.flush();

                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;
                    while ((bytesRead = fileData.read(buffer)) != -1) {
                        out.write(buffer, 0, bytesRead);
                    }
                    out.flush();

                    out.writeBytes(lineEnd);
                    out.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    out.flush();

                    readFromStreamAndDispatch(conn.getInputStream(), conn.getResponseCode());

                    conn.disconnect();
                } catch (IOException ex) {
                    ex.printStackTrace();

                    try {
                        readFromStreamAndDispatch(conn.getErrorStream(), conn.getResponseCode());
                    } catch (IOException exc) {
                        exc.printStackTrace();
                        dispatchError(STATUS_CONNECTION_ERROR);
                    }
                } finally {
                    conn.disconnect();
                }

                requestThread = null;
            }
        });
        requestThread.start();
    }

    @Override
    protected void dispatchSuccess(byte[] data, int code) {
        isLoading = false;
        if (listener != null) {
            listener.onRetainResponse(this, code, data);
        }
    }

    @Override
    protected void dispatchError(int code) {
        isLoading = false;
        if (listener != null) {
            listener.onRetainFailed(this, code);
        }
    }

    public void setListener(RetainerListener listener) {
        this.listener = listener;
    }

    public void setMethod(RetainerMethod method) {
        this.method = method;
    }

    public void retain(String urlString, String content) {
        if (isLoading) {
            cancel();
        }

        Crashlytics.log(Log.DEBUG, "Retainer", "Retainer retaining URL: " + urlString);
        Crashlytics.log(Log.DEBUG, "Retainer", "Retainer retaining content: " + content);
        retainWorker(urlString, content);
    }

    public void retain(String urlString, String fieldName, String fileName, InputStream data) {
        if (isLoading) {
            cancel();
        }

        Crashlytics.log(Log.DEBUG, "Retainer", "Retainer retaining URL: " + urlString);
        Crashlytics.log(Log.DEBUG, "Retainer", "Retainer retaining file...");
        retainerWorkerFile(urlString, fieldName, fileName, data);
    }

    public void cancel() {
        if (isLoading) {
            if (requestThread != null &&
                    !requestThread.isInterrupted()) {
                requestThread.interrupt();
                requestThread = null;
            }
            isLoading = false;
        }
    }
}
