package br.com.insanitech.io.crypto;

import android.util.Base64;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by anderson on 29/11/15.
 */
public class Encoder {
    public static String stringHash(String input) {
        return computeSHAHash(input);
    }

    private static String convertToHex(byte[] data) throws java.io.IOException {
        return Base64.encodeToString(data, 0, data.length, 0);
    }

    private static String computeSHAHash(String password) {
        MessageDigest mdSha1;
        try {
            mdSha1 = MessageDigest.getInstance("SHA-1");
            mdSha1.update(password.getBytes("ASCII"));
            byte[] data = mdSha1.digest();
            return convertToHex(data);
        } catch (NoSuchAlgorithmException | NullPointerException | IOException ex) {
            ex.printStackTrace();
            Crashlytics.logException(ex);
        }
        return null;
    }
}
