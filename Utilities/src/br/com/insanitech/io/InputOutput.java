package br.com.insanitech.io;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import br.com.insanitech.list.KeyValueList;
import br.com.insanitech.list.KeyValuePair;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

public class InputOutput {
	// reads resources regardless of their size
	public static byte[] getResource(int id, Context context) throws IOException {
		Resources resources = context.getResources();
		InputStream is = resources.openRawResource(id);

		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		byte[] readBuffer = new byte[4 * 1024];
		byte[] output;

		try {
			int read;
			do {
				read = is.read(readBuffer, 0, readBuffer.length);
				if (read == -1) {
					break;
				}
				bout.write(readBuffer, 0, read);
			} while (true);

			output = bout.toByteArray();
		} finally {
			is.close();
			bout.close();
		}

		return output;
	}

	// reads a string resource
	public static String getStringResource(int id, Charset encoding, Context ctx) throws IOException {
		return new String(getResource(id, ctx), encoding);
	}

	// reads an UTF-8 string resource
	public static String getStringResource(int id, Context ctx) throws IOException {
		return new String(getResource(id, ctx), Charset.forName("UTF-8"));
	}

	public static byte[] getFile(String name, Context context) throws IOException {
		InputStream is = context.openFileInput(name);

		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		byte[] readBuffer = new byte[4 * 1024];
		byte[] output = null;

		try {
			int read;
			do {
				read = is.read(readBuffer, 0, readBuffer.length);
				if (read == -1) {
					break;
				}
				bout.write(readBuffer, 0, read);
			} while (true);

			output = bout.toByteArray();
		} finally {
			is.close();
			bout.close();
		}

		return output;
	}

	public static String getStringFile(String name, Charset encoding, Context ctx) throws IOException {
		return new String(getFile(name, ctx), encoding);
	}

	public static String getStringFile(String name, Context ctx) throws IOException {
		return new String(getFile(name, ctx), Charset.forName("UTF-8"));
	}

	public static void saveStringFile(String name, String content, Context ctx) throws IOException {
		if (content != null && content.length() > 0) {
			File file = ctx.getFileStreamPath(name);
			if (file.exists()) {
				ctx.deleteFile(name);
			}
			FileOutputStream output = ctx.openFileOutput(name, Context.MODE_PRIVATE);
			output.write(content.getBytes());
			output.flush();
			output.close();
		}
	}

	public static void saveFile(String name, byte[] content, Context ctx) throws IOException {
		if (content != null && content.length > 0) {
			File file = ctx.getFileStreamPath(name);
			if (file.exists()) {
				ctx.deleteFile(name);
			}
			FileOutputStream output = ctx.openFileOutput(name, Context.MODE_PRIVATE);
			output.write(content);
			output.flush();
			output.close();
		}
	}

	public static void removeFile(String name, Context ctx) {
		ctx.deleteFile(name);
	}

	public static boolean fileExists(String name, Context ctx) {
		boolean exists = false;
		try {
			FileInputStream stream = ctx.openFileInput(name);
			exists = stream != null;
			if (stream != null) {
				stream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return exists;
	}

	// --------------------------------------------------- //

	public static String sendPostRequest(String uri, KeyValueList params) {
		Log.d("Send Post Request", "Uri string: " + uri + ", parameters: " + params.toString());
		String finalString = null;
		try {
			URL url = new URL(uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);

			String paramsString = "";
			for (int i = 0; i < params.size(); i++) {
				KeyValuePair pair = params.get(i);
				paramsString += pair.getName() + "="
						+ URLEncoder.encode(pair.getValue(), "UTF-8");
				if (i != params.size() - 1) {
					paramsString += "&";
				}
			}

			try {
				OutputStream output = conn.getOutputStream();
				output.write(paramsString.getBytes("UTF-8"));
			} catch (IOException e) {
				e.printStackTrace();
			}

			InputStream is = conn.getInputStream();

			BufferedReader in = new BufferedReader(new InputStreamReader(is));

			String inputLine = "";
			finalString = "";
			while ((inputLine = in.readLine()) != null) {
				finalString = finalString.concat(inputLine);
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return finalString;
	}

	public static String sendGetRequest(String uri) {
		Log.d("Send Get Request", "Url string: " + uri);
		String finalString = null;
		BufferedReader in = null;
		try {
			URL url = new URL(uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			InputStream is = conn.getInputStream();

			in = new BufferedReader(new InputStreamReader(is));

			String inputLine = "";
			finalString = "";
			while ((inputLine = in.readLine()) != null) {
				finalString = finalString.concat(inputLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return finalString;
	}

	public static Bitmap downloadImage(String url) {
		Log.d("Download Image", "Image Url string: " + url);
		InputStream input = null;
		try {
			URL uri = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) uri.openConnection();
			connection.setDoInput(true);
			connection.connect();
			input = connection.getInputStream();
			return BitmapFactory.decodeStream(input);
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (input != null) {
					input.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	// --------------------------------------------------- //

	public static String toUtf8String(byte[] bytes) {
		try {
			return new String(bytes, "UTF-8");
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}
		return "";
	}
}
