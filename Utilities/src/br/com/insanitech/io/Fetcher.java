package br.com.insanitech.io;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Fetcher extends Request {
    public interface FetcherListener {
        void onDataFetched(Fetcher fetcher, int code, byte[] data);

        void onFetchError(Fetcher fetcher, int code);
    }

    private FetcherListener listener;
    private Thread requestThread;

    public void fetchWorker(final String url) {
        isLoading = true;
        requestThread = new Thread(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection conn;
                try {
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setConnectTimeout(10000);
                    conn.setReadTimeout(10000);
                } catch (MalformedURLException ex) {
                    ex.printStackTrace();
                    dispatchError(STATUS_MALFORMED_URL);
                    return;
                } catch (IOException ex) {
                    ex.printStackTrace();
                    dispatchError(STATUS_CONNECTION_ERROR);
                    return;
                }

                    addRequestHeaders(conn);

                try {
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();

                    readFromStreamAndDispatch(conn.getInputStream(), conn.getResponseCode());
                } catch (IOException ex) {
                    ex.printStackTrace();

                    try {
                        readFromStreamAndDispatch(conn.getErrorStream(), conn.getResponseCode());
                    } catch (IOException exc) {
                        exc.printStackTrace();
                        dispatchError(STATUS_CONNECTION_ERROR);
                    }
                } finally {
                    conn.disconnect();
                }
                requestThread = null;
            }
        });
        requestThread.start();
    }

    @Override
    protected void dispatchSuccess(byte[] data, int code) {
        isLoading = false;
        if (listener != null) {
            listener.onDataFetched(this, code, data);
        }
    }

    @Override
    protected void dispatchError(int code) {
        isLoading = false;
        if (listener != null) {
            listener.onFetchError(this, code);
        }
    }

    public void setListener(FetcherListener listener) {
        this.listener = listener;
    }

    public void fetch(String urlString) {
        if (isLoading) {
            cancel();
        }

        Crashlytics.log(Log.DEBUG, "Fetcher", "Fetcher fetching URL: " + urlString);
        fetchWorker(urlString);
    }

    public void cancel() {
        if (isLoading) {
            if (requestThread != null &&
                    !requestThread.isInterrupted()) {
                requestThread.interrupt();
                requestThread = null;
            }
            isLoading = false;
        }
    }
}
