package br.com.insanitech.security;

import org.json.JSONException;
import org.json.JSONObject;
import br.com.insanitech.io.InputOutput;
import br.com.insanitech.list.KeyValueList;
import br.com.insanitech.ui.UserInterface;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;

public class Protector {
	public interface ProtectorResponse {
		void onProtectorResponse(boolean hasResponse, boolean permitted);
	}

	private static Protector sharedInstance = null;

	public static Protector sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new Protector();
		}

		return sharedInstance;
	}

	private Protector() {

	}

	@SuppressLint("NewApi")
	public void verifyPermission(final Integer appId, final ProtectorResponse responseCallback) {
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				KeyValueList params = new KeyValueList();
				params.add("id", appId);
				String response = InputOutput.sendPostRequest(
						"http://amponair.com.br/blocker/api/", params);
				if (response != null) {
					try {
						JSONObject obj = new JSONObject(response);
						if (responseCallback != null) {
							responseCallback.onProtectorResponse(true,
								obj.getInt("status") != 406);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					if (responseCallback != null) {
						responseCallback.onProtectorResponse(false, true);
					}
				}
			}
		};

		if (Build.VERSION.SDK_INT >= 11) {
			AsyncTask.execute(runnable);
		} else {
			new AsyncTask<String, Integer, String>() {

				@Override
				protected String doInBackground(String... params) {
					runnable.run();

					return null;
				}

			}.execute();
		}

	}

	public boolean manageResponse(final Activity currentActivity, boolean hasResponse, boolean permitted) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(currentActivity);
		boolean permittedOld = prefs.getBoolean("permitted", permitted);
		if (!hasResponse) {
			if (!permittedOld) {
				currentActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						showAlert(currentActivity);
					}
				});
			}
		} else {
			prefs.edit().putBoolean("permitted", permitted).apply();
			if (!permitted) {
				currentActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						showAlert(currentActivity);
					}
				});
			}
		}
		
		return permittedOld || permitted;
	}

	private void showAlert(Activity activity) {
		UserInterface.createAlert(activity, getAppLabel(activity),
				"Você não tem mais permissão para utilizar esta aplicação!",
				"Fechar", new Runnable() {
					@Override
					public void run() {
						System.exit(0);
					}
				}).show();
	}

	private String getAppLabel(Context pContext) {
		PackageManager lPackageManager = pContext.getPackageManager();
		ApplicationInfo lApplicationInfo = null;
		try {
			lApplicationInfo = lPackageManager.getApplicationInfo(
				pContext.getApplicationInfo().packageName, 0);
		} catch (final NameNotFoundException e) {
		}
		return (String) (lApplicationInfo != null ? lPackageManager.getApplicationLabel(lApplicationInfo) : "Unknown");
	}
}
